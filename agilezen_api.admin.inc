<?php
/**
 * @file
 * Agile Zen API administration
 * 
 * Set URL of destination webservice, and private key
 */

/**
 * Define form for administration page
 */
function agilezen_api_settings() {
  $form = array();
  $form['agilezen_api_project_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Project ID'),
    '#default_value' => variable_get('agilezen_api_project_id', ''),
    '#description' => t('Enter the ID of the project or board to interact with'),
  );
  $form['agilezen_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Private Key'),
    '#default_value' => variable_get('agilezen_api_key', 'abc'),
    '#description' => t('Enter the URL of the private key used to verify the authenticity of outgoing messages'),
  );
  $form['agilezen_api_log_everything'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log all messages?'),
    '#default_value' => variable_get('agilezen_api_log_everything', 0),
    '#description' => t('Enable to log successful as well as failed messages. Only recommend for debugging or development, as this will create a large volume of log messages'),
  );
  return system_settings_form($form);
}

/**
 * -Test by sending sample values to external service
 */
function agilezen_api_test($form_state) {
  $form = array();
  $form['agilezen_api_test_action'] = array(
    '#type' => 'select',
    '#title' => t('Action'),
    '#description' => t('Select the action to take.'),
    '#options' => array(
      'list' => 'List',
      'load' => 'Load',
      'delete' => 'Delete',
      'create' => 'Create',
      'update' => 'Update',
    ),
  );
  $form['agilezen_api_test_story_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Story ID to retrieve'),
    '#description' => t('Enter a Story ID to retrieve it from Agile Zen'),
  );
  $form['agilezen_api_test_story_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Story Title'),
    '#description' => t('Enter a Story Title'),
  );
  $form['agilezen_api_test_story_phase'] = array(
    '#type' => 'select',
    '#title' => t('Story Phase'),
    '#description' => t('Select the phase to place the story in.'),
    '#options' => array(
      275772 => 'Queue',
      275773 => 'Scoping',
      275774 => 'Development',
      275775 => 'Testing',
      275777 => 'Launch',
      275776 => 'Complete',
    ),
  );
  $form['agilezen_api_test_story_color'] = array(
    '#type' => 'select',
    '#title' => t('Action'),
    '#description' => t('Select the action to take.'),
    '#options' => array(
      'blue' => 'Blue',
      'red' => 'Red',
      'green' => 'Green',
      'orange' => 'Orange',
      'yellow' => 'Yellow',
      'purple' => 'Purple',
      'teal' => 'Teal',
    ),
  );
  $form['agilezen_api_test_response_format'] = array(
    '#type' => 'select',
    '#title' => t('Response formatting'),
    '#description' => t('Select the type of formatting to display the response'),
    '#options' => array(
      'results' => 'Results',
      'full' => 'Full',
    ),
  );
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );
  return $form;
}

/**
 * -Submit handler for test message  form
 */
function agilezen_api_test_submit($form, &$form_state) {

  // Variables
  $story_id = $form_state['values']['agilezen_api_test_story_id'];
  $action = $form_state['values']['agilezen_api_test_action'];
  $format = $form_state['values']['agilezen_api_test_response_format'];

  // Story Object
  if (in_array($action, array('save', 'create', 'update'))) {
    $story = array(
      'text' => $form_state['values']['agilezen_api_test_story_title'],
      'phase' => array(
        'id' => $form_state['values']['agilezen_api_test_story_phase'],
      ),
      'color' => $form_state['values']['agilezen_api_test_story_color'],
    );
    if ($action == 'update') {
      $story['id'] = $form_state['values']['agilezen_api_test_story_id'];
    }
  }
  $story = (Object) $story;

  // Submit requests
  switch ($action) {
    case 'list':
      $response = agilezen_rest_api_stories(array(), $format);
    break;
    case 'load':
      $response = agilezen_rest_api_story_load($story_id, $format);
    break;
    case 'delete':
      $response = agilezen_rest_api_story_delete($story_id, $format);
    break;
    case 'create':
      $response = agilezen_rest_api_story_create($story, $format);
    break;
    case 'update':
      $response = agilezen_rest_api_story_update($story, $format);
    break;
  }

  // Print response
  if ($format == 'results') kpr($response); // graphical 
  if ($format == 'full') dpr($response); // text
  exit(); 
}
