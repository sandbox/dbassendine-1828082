<?php 

/**
 * @file 
 * Agile Zen API Classes
 * 
 */

/**
 * Set URL Endpoint and Authentication
 */
class agilezen_api_query extends rest_api_query {
  // URL / Endpoint
  protected $scheme = 'https';
  protected $host = 'agilezen.com';
  protected $path = '/api/v1'; // include starting slash

  // Agile Zen does not BasicAuth username and password
  protected $password = FALSE;
  protected $username = FALSE;

  function __construct() {
    // Find the project / board ID and append to base path
    $project_id = variable_get('agilezen_api_project_id', FALSE);
    if ($project_id) $this->path .= '/projects/'. $project_id;

    // Find API Key and set the Authentication Header
    $api_key = variable_get('agilezen_api_key', FALSE);
    $this->headers['X-Zen-ApiKey'] = $api_key;

    // Only proceed if API Key is set
    $this->initialized = !empty($api_key);
  }
}
